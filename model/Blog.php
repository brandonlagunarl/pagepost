<?php
class Blog extends EntidadBase{

    private $bgp_id;
    private $bgp_title;
    private $bgp_content;
    private $bgp_autor;
    private $bgp_tags;
    private $bgp_comments;
    private $bgp_url_cover;
    private $bgp_cover_type;
    private $bgp_url_redirect;

    public function __construct($adapter){
        $table = "business_blog_pages";
        parent:: __construct($table, $adapter);
    }

    public function getbgp_id()
    {
        return $this->bgp_id;
    }
    public function setbgp_id($bgp_id)
    {
        $this->bgp_id = $bgp_id;
    }
    
    public function getbgp_title()
    {
        return $this->bgp_title;
    }
    public function setbgp_title($bgp_title)
    {
        $this->bgp_title = $bgp_title;
    }

    public function getbgp_content()
    {
        return $this->bgp_content;
    }
    public function setbgp_content($bgp_content)
    {
        $this->bgp_content = $bgp_content;
    }

    public function getautor()
    {
        return $this->autor;
    }
    public function setautor($autor)
    {
        $this->autor = $autor;
    }

    public function getbgp_tags()
    {
        return $this->bgp_tags;
    }
    public function setbgp_tags($bgp_tags)
    {
        $this->bgp_tags = $bgp_tags;
    }

    public function getbgp_comments()
    {
        return $this->bgp_comments;
    }
    public function setbgp_comments($bgp_comments)
    {
        $this->bgp_comments = $bgp_comments;
    }

    public function getbgp_url_cover()
    {
        return $this->bgp_url_cover;
    }
    public function setbgp_url_cover($bgp_url_cover)
    {
        $this->bgp_url_cover = $bgp_url_cover;
    }

    public function getbgp_cover_type()
    {
        return $this->bgp_cover_type;
    }
    public function setbgp_cover_type($bgp_cover_type)
    {
        $this->bgp_cover_type = $bgp_cover_type;
    }

    public function getbgp_url_redirect()
    {
        return $this->bgp_url_redirect;
    }
    public function setbgp_url_redirect($bgp_url_redirect)
    {
        $this->bgp_url_redirect = $bgp_url_redirect;
    }

    public function getBlog()
    {
        $query = $this->db()->query("SELECT * FROM business_blog_pages INNER JOIN post_autors ON bgp_autor = post_autors.pa_id INNER JOIN post_tags ON bgp_tags = post_tags.pt_id ORDER BY bgp_id DESC");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
    }

    public function getBlogByTag($tag)
    {
        $tags = $this->db()->query("SELECT * FROM post_tags WHERE pt_name = '$tag'");
        if($tags->num_rows > 0){
            while($row = $tags->fetch_assoc()) {
                $id = $row["pt_id"];
            }
            $query = $this->db()->query("SELECT * FROM business_blog_pages INNER JOIN post_autors ON bgp_autor = post_autors.pa_id INNER JOIN post_tags ON bgp_tags = post_tags.pt_id WHERE bgp_tags = $id ORDER BY RAND() ");
                if($query->num_rows > 0){
                    while ($row = $query->fetch_object()) {
                    $resultSet[]=$row;
                }
                }else{
                    $resultSet =[];
                }
        }else{
            $resultSet = [];

        }
        
        return $resultSet;

        
    }

    public function getBlogById($id)
    {
        $query = $this->db()->query("SELECT * FROM business_blog_pages INNER JOIN post_autors ON bgp_autor = post_autors.pa_id INNER JOIN post_tags ON bgp_tags = post_tags.pt_id  WHERE bgp_id = $id");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }         
        }else{
            $resultSet=[];
        }
        return $resultSet;
    }

    public function otherArticles($limit)
    {
        $query = $this->db()->query("SELECT * FROM business_blog_pages INNER JOIN post_autors ON bgp_autor = post_autors.pa_id INNER JOIN post_tags ON bgp_tags = post_tags.pt_id ORDER BY RAND() LIMIT $limit");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
        }else{
            $resultSet =[];
        }
        return $resultSet;
    }

    public function relatedArticles($related)
    {
        $query = $this->db()->query("SELECT * FROM business_blog_pages INNER JOIN post_autors ON bgp_autor = post_autors.pa_id INNER JOIN post_tags ON bgp_tags = post_tags.pt_id WHERE bgp_tags = $related");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
        }else{
            $resultSet =[];
        }
        return $resultSet;
    }

}

?>