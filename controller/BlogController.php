<?php
class BlogController extends ControladorBase{
    public $conectar;
	public $adapter;
	
    public function __construct() {
        parent::__construct();
		 
        $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
        
    }

    public function Index()
    {
        $alltags= new PostTags($this->adapter);
        $tags = $alltags->getAll();

        $otherArticlesClass = new Blog($this->adapter);
        $otherArticles = $otherArticlesClass->otherArticles(3);

        if(!empty($_GET['s'])){
            $search = $_GET['s'];
            $blog= new Blog($this->adapter);
            $posts = $blog->getBlogByTag($search);
            foreach($posts as $related){
            $related= $blog->relatedArticles($related->bgp_tags);
            }

        }else{
        $blog= new Blog($this->adapter);
        $posts = $blog->getBlog();
        }

    $this->view("blog/index",array("posts"=>$posts, "tags" => $tags,"otherArticles"=>$otherArticles));
    }

    public function read()
    {
        
        $alltags= new PostTags($this->adapter);
        $tags = $alltags->getAll();

        $otherArticlesClass = new Blog($this->adapter);
        $otherArticles = $otherArticlesClass->otherArticles(3);

        if(!empty($_GET['s'])){
            $search = $_GET['s'];
            $blog= new Blog($this->adapter);
            $posts = $blog->getBlogByid($search);
            $media = new ImageBlog($this->adapter);
            $gallery = $media->getImages($search);
            foreach($posts as $related){}
            $related= $blog->relatedArticles($related->bgp_tags);

            if(empty($posts)){
                $this->redirect("Index","");
            }
        }
        else{
            $this->redirect("blog","");
        }
        $this->view("blog/read",array("posts"=>$posts,"gallery"=>$gallery,"tags"=>$tags,"otherArticles"=>$otherArticles,"related"=>$related));
    }

}
?>