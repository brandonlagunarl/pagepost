
<div class=container-fluid style="background-image:url('media/img/2043687.jpg'); height:100%; background-size:cover;">
    <div class="container">
    <div class="jumbotron" style="background:transparent;">
        <div class=row>
            <div class="col col-sm-12 col-md-6" style="background:#FFAA2D;color:white;">
                <?php $this->frameview("login/form/clasicForm",array())?>
                <?php if(isset($alert)){$this->frameview("toast/alert",array("alert"=>$alert));}?>
            </div>
            <div class="col col-sm-12 col-md-1" style="background:#FFAA2D;color:white;text-align:center;">
                <h4>or</h4>
            </div>
            <div class="col col-sm-12 col-md-5" style="background:#008993;color:white;">
                <?php $this->frameview("login/form/clasicRegister",array())?>
            </div>
        </div>
    </div>
    </div>
   
</div>

<script type="text/javascript">
window.onload = function() {
    $('.toast').toast('show')
};

</script>