<form action="Login?action=authentication" method="post">
  <div class="form-group">
    <h3>Log In</h3>
  </div>
  <div class="form-group">
    <label for="usr_username">Email address</label>
    <input type="email" class="form-control" id="usr_username" name="usr_username"aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="usr_password">Password</label>
    <input type="password" class="form-control" id="usr_password" name="usr_password">
  </div>
  <button type="submit" class="btn btn-primary">Join</button>
</form>