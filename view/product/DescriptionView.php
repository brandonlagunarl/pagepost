<?php foreach($allproducts as $product) {}?>
<style>
img {
  width: 350px
}
.imageView:hover {
  cursor: zoom-in;
}
a:link{}
.hero-image {
  position: relative;
  background: url(<?="media/img/".$product->ImgProduct?>) no-repeat center center;
  background-size: 60%;
    width: 100%;
    height: 500px;
    z-index: 0;
    background-position-y:-300px;
}

.breadcrumb {
  margin: 2em 0;
  list-style: none;
  overflow: hidden;
}

.breadcrumb li:before {
  font-family: FontAwesome;
  content: "\f105";
  font-size: 10px;
  line-height: 4;
  padding: 0 0.8em;
  color: #ccc;
}

.breadcrumb li {
  float: left;
}

.breadcrumb li a {
  font-size: 0.86em;
  font-weight: 100;
  color: #bbb;
  text-decoration: none;
  padding: 10px 0 10px 6px;
  position: relative;
  display: block;
  float: left;
}

.wrapper {
  display: table;
  max-width: 1200px;
  margin: 0 auto;
  padding-top: 1em;
}

.inner {
  display: table-cell;
  padding: 0 4em;
  float: left;
  text-align: center;
}

.inner span {
  font-size: .9em;
  color: #aaa;
}

.imageView {
  border-style: solid;
  border-color: #eee;
  border-width: thin;
  width: 400px;
  height: 510px;
  z-index: 0;
  left: 100px;
  top: 100px;
  overflow: hidden;
  text-align: center;
}

.description {
  max-width: 600px;
  float: left;
}

.product-name {
  font-size: 2.3em;
  font-weight: 700;
  color: #cb8f0f;
}

.price h2 {
  color: #cb8f0f;
  letter-spacing: 1px;
}

.basket-btn {
  display: table;
  width: 200px;
  height: 60px;
  background-color: #222;
  color: #fff;
  margin: 3em 0;
  border: 1px solid #222;
  cursor: pointer;
  box-shadow: inset 0 0 0 0 #fff;
  -webkit-transition: all ease-out 0.2s;
  -moz-transition: all ease-out 0.2s;
  transition: all ease-out 0.2s;
}
.basket-btn {
  display: table;
  width: 200px;
  height: 60px;
  background-color: #222;
  color: #fff;
  margin: 3em 0;
  border: 1px solid #222;
  cursor: pointer;
  box-shadow: inset 0 0 0 0 #fff;
  -webkit-transition: all ease-out 0.2s;
  -moz-transition: all ease-out 0.2s;
  transition: all ease-out 0.2s;
}

.basket-btn:after {}

.basket-btn:hover {
  box-shadow: inset 0 100px 0 0 #fff;
}

.basket-btn span:hover {
  color: #111;
}

.basket-btn span {
  display: table-cell;
  text-align: center;
  vertical-align: middle;
}
</style>
<title><?= $product->NameProduct." | ".$product->DescProduct; ?></title>

<div class="hero-image"></div>
<?php Paginator(); ?>
<div class="wrapper">
  <div class="inner">
    <div class="imageView">
      <div class="image"><img src="media/img/<?=$product->ImgProduct?>" meta="<?=$product->MetaKeyWord." ".$product->DescProduct;?>"/></div>

    </div> <span>Sostener para hacer zoom</span>
  </div>
  <div class="description">
    <div class="content">
      <span class="product-name"><?=$product->NameProduct?></span>
      <h2><?=$product->DescProduct?></h2>
      <p><?=$product->LongDescProduct?></p>
      <div class="price">
        <h2>$<?=$product->PriceProduct?> COP</h2></div>
        <a class="padd" id="padd_sdk_<?=$product->id?>"><div class="basket-btn"><span>COMPRAR PRODUCTO</span></div></a>
</div>
</div>
</div>
<script>
function mouseMoveMatrix() {
  var train = $('.image');

  train.on('mousedown', function() {
    train.css({
      'transform': 'scale(2.2)'
    })
  })

  .on('mouseup', function() {
    train.css({
      'transform': 'scale(1)'
    })
  })

  .on('mousemove', function(e) {
    $('.image').css({
      'transform-origin': ((e.pageX - $(this).offset().left) / $(this).width()) * 50 + '% ' + ((e.pageY - $(this).offset().top) / $(this).height()) * 50 + '%'
    });
  });
  $('.image').on('dragstart', function(event) {
    event.preventDefault();
  });
}

mouseMoveMatrix();

// function myFunction(x) {
//   x.classList.toggle("change");
// }

// Parallax images

$(window).scroll(function(e) {
  parallax();
});

function parallax() {
  var scrolled = $(window).scrollTop();
  $('.strip').css('top', -(scrolled * 0.1) + 'px');
  $('.feature-image img').css('top', -(scrolled * 0.1) + 'px');
  $('.hero-image').css('top', -(scrolled * 0.1) + 'px');
}

function openNav() {

  $('body').addClass('noscroll');
  $('.nav').addClass('nav-active');
  $('.overlay').addClass('overlay-show');
  $('.sidenav').addClass('snav-active');

}

function closeNav() {
  $('body').removeClass('noscroll');
  $('.nav').removeClass('nav-active');
  $('.overlay').removeClass('overlay-show');
  $('.sidenav').removeClass('snav-active');

}
</script>

<script src='https://use.fontawesome.com/048010cf41.js'></script>