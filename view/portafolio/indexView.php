
        <div class="attorna-page-title-wrap  attorna-style-custom attorna-center-align">
            <div class=attorna-header-transparent-substitute></div>
            <div class=attorna-page-title-overlay></div>
            <div class="attorna-page-title-container attorna-container">
                <div class="attorna-page-title-content attorna-item-pdlr">
                    <h1 class="attorna-page-title">Portfolio 4 Columns</h1>
                    <div class=attorna-page-caption>Full Width, With Excerpt & Space</div>
                </div>
            </div>
        </div>
        <div class=attorna-page-wrapper id=attorna-page-wrapper>
            <div class=gdlr-core-page-builder-body>
                <div class="gdlr-core-pbf-wrapper " style="padding: 60px 15px 30px 15px;">
                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full">
                            <div class=gdlr-core-pbf-element>
                                <div class="gdlr-core-portfolio-item gdlr-core-item-pdb clearfix  gdlr-core-portfolio-item-style-grid" style="padding-bottom: 10px ;">
                                    <div class="filter clearfix gdlr-core-filterer-wrap gdlr-core-js  gdlr-core-item-pdlr gdlr-core-style-text gdlr-core-center-align">
                                        <ul>
                                            <li><a href="#" class="active" data-filter="*">All</a></li>
                                            <li><a href="#" data-filter=".class1" >Accidental</a></li>
                                            <li><a href="#" data-filter=".class2" >Financial</a></li>
                                            <li><a href="#" data-filter=".class3" >Violence</a></li>
                                        </ul>
                                    </div>   


                                    <div class="gdlr-core-portfolio-item-holder gdlr-core-js-2 clearfix filter-container" data-layout=masonry>


                                        <div class="gdlr-core-item-list class1 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Family1.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Family1-700x660.jpg alt width=700 height=660 title=Family1><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Family Violence</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Violence</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class2 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Giving-Million-Air-Its-Wings.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Giving-Million-Air-Its-Wings-700x660.jpg alt width=700 height=660 title=Giving-Million-Air-Its-Wings><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Giving Million Air Its Wings</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Financial</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class3 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Reinventing-the-Riverfront.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Reinventing-the-Riverfront-700x660.jpg alt width=700 height=660 title=Reinventing-the-Riverfront><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Car Accident Insurance</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Accidental</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class1 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Making-Sure-the-Closing-Happens.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Making-Sure-the-Closing-Happens-700x660.jpg alt width=700 height=660 title=Making-Sure-the-Closing-Happens><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Making Sure It&#8217;s Closed</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Financial</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class2 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Public-Company-Fraud.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Public-Company-Fraud-700x660.jpg alt width=700 height=660 title=Public-Company-Fraud><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Public Company Fraud</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Financial</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class3 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Nighmare-on-Wall-Street.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Nighmare-on-Wall-Street-700x660.jpg alt width=700 height=660 title=Nighmare-on-Wall-Street><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Nighmare on Wall Street</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Violence</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class1 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/Privacy-Matter.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/Privacy-Matter-700x660.jpg alt width=700 height=660 title=Privacy-Matter><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >Privacy Matter</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Financial</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="gdlr-core-item-list class2 gdlr-core-item-pdlr gdlr-core-column-15">
                                            <div class="gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal" style="margin-bottom: 25px ;">
                                                <div class="gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon">
                                                    <div class="gdlr-core-portfolio-thumbnail-image-wrap  gdlr-core-zoom-on-hover">
                                                        <a class="gdlr-core-lightgallery gdlr-core-js " href=upload/MaTix-Tax-Invation.jpg data-lightbox-group=gdlr-core-img-group-1><img src=upload/MaTix-Tax-Invation-700x660.jpg alt width=700 height=660 title=MaTix-Tax-Invation><span class="gdlr-core-image-overlay  gdlr-core-portfolio-overlay gdlr-core-image-overlay-center-icon gdlr-core-js gdlr-core-with-margin"><span class=gdlr-core-image-overlay-content ><span class=gdlr-core-portfolio-icon-wrap ><i class="gdlr-core-portfolio-icon arrow_expand" ></i></span></span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="gdlr-core-portfolio-content-wrap gdlr-core-skin-divider">
                                                    <h3 class="gdlr-core-portfolio-title gdlr-core-skin-title" style="font-size: 16px ;font-weight: 600 ;letter-spacing: 0px ;text-transform: none ;"><a href=# >MaTix Tax Invation</a></h3><span class="gdlr-core-portfolio-info gdlr-core-portfolio-info-tag gdlr-core-info-font gdlr-core-skin-caption"><a href=# rel=tag>Accidental</a></span>
                                                    <div class=gdlr-core-portfolio-content></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type='text/javascript' src='js/isotope.js'></script>