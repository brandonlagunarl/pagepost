<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
  <div class="toast-header">
    <i class="fas fa-bell"></i>
    <strong class="mr-auto">Message</strong>
    <small>Just Now</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="toast-body" style="color:gray;">
    <?=$alert?>
  </div>
</div>