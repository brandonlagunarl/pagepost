<div class=attorna-page-wrapper id=attorna-page-wrapper>
            <div class=attorna-not-found-wrap id=attorna-full-no-header-wrap>
                <div class=attorna-not-found-background></div>
                <div class="attorna-not-found-container attorna-container">
                    <div class=attorna-header-transparent-substitute></div>
                    <div class="attorna-not-found-content attorna-item-pdlr">
                        <h1 class="attorna-not-found-head">404</h1>
                        <h3 class="attorna-not-found-title attorna-content-font">Pagina No Encontrada</h3>
                        <div class=attorna-not-found-caption>PERDIDO EN EL <span>SPACIO</span> <?=strtoupper($_GET['controller'])?>? Hmm, parece que esa página no existe.</div>
                        <form role=search method=get class=search-form action=#>
                        <input type=text class="search-field attorna-title-font" placeholder="Buscas algo especifico?" value name=s>
                        <div class=attorna-top-search-submit><i class="fa fa-search"></i></div>
                        <input type=submit class=search-submit value=Search>
                        </form>
                        <div class=attorna-not-found-back-to-home><a href=index>Ir a Casa</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>