<div class="attorna-page-title-wrap  attorna-style-custom attorna-center-align">
            <div class=attorna-header-transparent-substitute></div>
            <div class=attorna-page-title-overlay></div>
            <div class="attorna-page-title-container attorna-container">
                <div class="attorna-page-title-content attorna-item-pdlr">
                    <h1 class="attorna-page-title">Bienvenido a Grancolombiano Blog</h1>
                    <div class=attorna-page-caption>Mantengase informado de sus derechos</div>
                </div>
            </div>
</div>


<div class=attorna-page-wrapper id=attorna-page-wrapper>
    <div class=gdlr-core-page-builder-body>
        <div class="gdlr-core-pbf-sidebar-wrapper ">
            <div class="gdlr-core-pbf-sidebar-container gdlr-core-line-height-0 clearfix gdlr-core-js gdlr-core-container">
                <div class="gdlr-core-pbf-sidebar-content  gdlr-core-column-40 gdlr-core-pbf-sidebar-padding gdlr-core-line-height gdlr-core-column-extend-left" style="padding: 60px 10px 30px 0px;">
                    <div class=gdlr-core-pbf-background-wrap style="background-color: #f7f7f7 ;"></div>
                    <div class=gdlr-core-pbf-sidebar-content-inner>
                        <div class=gdlr-core-pbf-element>
                            <div class="gdlr-core-blog-item gdlr-core-item-pdb clearfix  gdlr-core-style-blog-full-with-frame" style="padding-bottom: 40px ;">
                                <div class="gdlr-core-blog-item-holder gdlr-core-js-2 clearfix" data-layout=fitrows>
                                    
                                    <?php
                                    //blog medium view
                                    
                                        $this->frameview("blog/MediumBlogPost",array("posts"=>$posts));
                                    
                                    
                                    ?>
                                    
                                </div>
                                <div class="gdlr-core-pagination  gdlr-core-style-round gdlr-core-left-align gdlr-core-item-pdlr"><a class href=#>Cargar Mas</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gdlr-core-pbf-sidebar-right gdlr-core-column-extend-right  attorna-sidebar-area gdlr-core-column-20 gdlr-core-pbf-sidebar-padding  gdlr-core-line-height" style="padding: 80px 0px 30px 0px;">
                    <div class=gdlr-core-pbf-background-wrap style="background-color: #f7f7f7 ;"></div>

                    <div class="gdlr-core-sidebar-item gdlr-core-item-pdlr">
                        <?php $this->frameview("blog/frames/otherArticles",array("otherArticles"=>$otherArticles))?>
                        
                        <div id=tag_cloud-1 class="widget widget_tag_cloud attorna-widget">
                            <h3 class="attorna-widget-title">Tag Cloud</h3>
                            <span class=clear></span>
                            <div class=tagcloud>
                                <a href=# class="tag-cloud-link tag-link-86 tag-link-position-1" style="font-size: 18.5pt;" aria-label="Antitrust (3 items)">Antitrust</a>
                                <a href=# class="tag-cloud-link tag-link-92 tag-link-position-2" style="font-size: 8pt;" aria-label="Bankruptcy (1 item)">Bankruptcy</a>
                                <a href=# class="tag-cloud-link tag-link-83 tag-link-position-3" style="font-size: 14.3pt;" aria-label="Employment (2 items)">Employment</a>
                                <a href=# class="tag-cloud-link tag-link-81 tag-link-position-4" style="font-size: 22pt;" aria-label="Family (4 items)">Family</a>
                                <a href=# class="tag-cloud-link tag-link-85 tag-link-position-5" style="font-size: 18.5pt;" aria-label="General Practice (3 items)">General Practice</a>
                                <a href=# class="tag-cloud-link tag-link-91 tag-link-position-6" style="font-size: 8pt;" aria-label="Immigration (1 item)">Immigration</a>
                                <a href=# class="tag-cloud-link tag-link-82 tag-link-position-7" style="font-size: 8pt;" aria-label="Law (1 item)">Law</a>
                                <a href=# class="tag-cloud-link tag-link-99 tag-link-position-8" style="font-size: 8pt;" aria-label="Nonprofit (1 item)">Nonprofit</a>
                                <a href=# class="tag-cloud-link tag-link-87 tag-link-position-9" style="font-size: 8pt;" aria-label="Personal Injury (1 item)">Personal Injury</a>
                                <a href=# class="tag-cloud-link tag-link-100 tag-link-position-10" style="font-size: 18.5pt;" aria-label="Property (3 items)">Property</a>
                            </div>
                        </div>
                        <div id=text-4 class="widget widget_text attorna-widget">
                            <?php $this->frameview("blog/frames/blogDescription",array())?>
                        </div>
                        <div id=gdlr-core-custom-menu-widget-5 class="widget widget_gdlr-core-custom-menu-widget attorna-widget">
                            <h3 class="attorna-widget-title">Areas Practicas</h3><span class=clear></span>
                            <div class=menu-practice-areas-container>
                                <ul id=menu-practice-areas class="gdlr-core-custom-menu-widget gdlr-core-menu-style-list">
                                <li class="menu-item"><a href="Blog">Todas</a></li>
                                <?php foreach($tags as $tags){?>
                                    <li class="menu-item"><a href="?s=<?=$tags->pt_name?>"><?=$tags->pt_name?></a></li>
                                <?php } ?>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=gdlr-core-pbf-section>
            <div class="gdlr-core-pbf-section-container gdlr-core-container clearfix">
                <div class="gdlr-core-pbf-column gdlr-core-column-12 gdlr-core-column-first">
                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js ">
                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js "></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>