<div class=attorna-page-wrapper id=attorna-page-wrapper>
            <?php $this->frameview("blog/head/headerTitle",array("posts"=>$posts))?>
            <div class="attorna-content-container attorna-container  gdlr-core-sticky-sidebar gdlr-core-js">
                <div class=" attorna-sidebar-wrap clearfix attorna-line-height-0 attorna-sidebar-style-right">
                    <div class=" attorna-sidebar-center attorna-column-40 attorna-line-height">
                        <div class="attorna-content-wrap attorna-item-pdlr clearfix">
                            <div class=attorna-content-area>
                                <?php $this->frameview("blog/areaPost",array("posts"=>$posts,"gallery"=>$gallery));?>
                            </div>

                            <div class="attorna-page-builder-wrap attorna-item-rvpdlr">
                                <div class=gdlr-core-page-builder-body></div>
                            </div>
                            
                            
                            
                            <div class="attorna-single-social-share clearfix">
                                <div class="gdlr-core-social-share-item gdlr-core-item-pdb  gdlr-core-left-align gdlr-core-social-share-left-text gdlr-core-style-plain" style="padding-bottom: 0px ;">
                                    <span class="gdlr-core-social-share-count gdlr-core-skin-title">
                                        <span class=gdlr-core-count >0</span>
                                        <span class=gdlr-core-suffix>Shares</span>
                                        <span class="gdlr-core-divider gdlr-core-skin-divider"></span>
                                    </span>
                                    <span class=gdlr-core-social-share-wrap>
                                        <a class=gdlr-core-social-share-facebook href="#" target=_blank >
                                            <i class="fa fa-facebook" ></i>
                                        </a>
                                        <a class=gdlr-core-social-share-google-plus href="#" target=_blank >
                                            <i class="fa fa-google-plus" ></i>
                                        </a>
                                        <a class=gdlr-core-social-share-pinterest href="#" target=_blank >
                                            <i class="fa fa-pinterest-p" ></i>
                                        </a>
                                        <a class=gdlr-core-social-share-twitter href="#" target=_blank  >
                                            <i class="fa fa-twitter" ></i>
                                        </a>
                                    </span>
                                </div>
                                <div class="attorna-single-magazine-author-tags clearfix"></div>
                            </div>
                            <div class=clear></div>

                            <?php $this->frameview("blog/frames/relatedPosts",array("related"=>$related))?>

                        </div>
                    </div>
                    <div class=" attorna-sidebar-right attorna-column-20 attorna-line-height attorna-line-height">
                        <div class="attorna-sidebar-area attorna-item-pdlr">
                            <?php $this->frameview("blog/frames/otherArticles",array("otherArticles"=>$otherArticles))?>
                            
                            <div id=tag_cloud-1 class="widget widget_tag_cloud attorna-widget">
                                <h3 class="attorna-widget-title">Tag Cloud</h3>
                                <span class=clear></span>
                                <div class=tagcloud>
                                    <a href=# class="tag-cloud-link tag-link-86 tag-link-position-1" style="font-size: 18.5pt;" aria-label="Antitrust (3 items)">Antitrust</a>
                                    <a href=# class="tag-cloud-link tag-link-92 tag-link-position-2" style="font-size: 8pt;" aria-label="Bankruptcy (1 item)">Bankruptcy</a>
                                    <a href=# class="tag-cloud-link tag-link-83 tag-link-position-3" style="font-size: 14.3pt;" aria-label="Employment (2 items)">Employment</a>
                                    <a href=# class="tag-cloud-link tag-link-81 tag-link-position-4" style="font-size: 22pt;" aria-label="Family (4 items)">Family</a>
                                    <a href=# class="tag-cloud-link tag-link-85 tag-link-position-5" style="font-size: 18.5pt;" aria-label="General Practice (3 items)">General Practice</a>
                                    <a href=# class="tag-cloud-link tag-link-91 tag-link-position-6" style="font-size: 8pt;" aria-label="Immigration (1 item)">Immigration</a>
                                    <a href=# class="tag-cloud-link tag-link-82 tag-link-position-7" style="font-size: 8pt;" aria-label="Law (1 item)">Law</a>
                                    <a href=# class="tag-cloud-link tag-link-99 tag-link-position-8" style="font-size: 8pt;" aria-label="Nonprofit (1 item)">Nonprofit</a>
                                    <a href=# class="tag-cloud-link tag-link-87 tag-link-position-9" style="font-size: 8pt;" aria-label="Personal Injury (1 item)">Personal Injury</a>
                                    <a href=# class="tag-cloud-link tag-link-100 tag-link-position-10" style="font-size: 18.5pt;" aria-label="Property (3 items)">Property</a>
                                </div>
                            </div>
                            <div id=text-4 class="widget widget_text attorna-widget">
                            <?php $this->frameview("blog/frames/blogDescription",array())?>
                            </div>
                            <div id=gdlr-core-custom-menu-widget-5 class="widget widget_gdlr-core-custom-menu-widget attorna-widget">
                                <h3 class="attorna-widget-title">Practice Areas</h3><span class=clear></span>
                                <div class=menu-practice-areas-container>
                                    <ul id=menu-practice-areas class="gdlr-core-custom-menu-widget gdlr-core-menu-style-list">
                                    <li class="menu-item"><a href="Blog">Todas</a></li>
                                    <?php foreach($tags as $tags){?>
                                        <li class="menu-item"><a href="?s=<?=$tags->pt_name?>"><?=$tags->pt_name?></a></li>
                                    <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
