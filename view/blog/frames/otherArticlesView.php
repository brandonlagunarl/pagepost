<div id=gdlr-core-recent-post-widget-2 class="widget widget_gdlr-core-recent-post-widget attorna-widget">
                                <h3 class="attorna-widget-title">Otros Articulos</h3><span class=clear></span>
                                <div class="gdlr-core-recent-post-widget-wrap gdlr-core-style-1">
                                <?php foreach($otherArticles as $article){?>
                                    <div class="gdlr-core-recent-post-widget clearfix">
                                        <div class="gdlr-core-recent-post-widget-thumbnail gdlr-core-media-image">
                                        <?php 
                                        $frameviews = "blog/frame".$article->bgp_cover_type;
                                        $this->frameview("$frameviews",array("cover"=>$article->bgp_url_cover,"w"=>"150","h"=>"150"));
                                        ?>
                                        </div>
                                        <div class=gdlr-core-recent-post-widget-content>
                                            <div class=gdlr-core-recent-post-widget-title><a href="Blog?action=read&s=<?=$article->bgp_id?>"><?=$article->bgp_title?></a></div>
                                            <div class=gdlr-core-recent-post-widget-info><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"><span class=gdlr-core-head ><i class=icon_clock_alt ></i></span><a href=#>Date</a></span><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-author"><span class=gdlr-core-head ><i class=icon_documents_alt ></i></span><a href=# title="Posts by <?=$article->pa_name?>" rel=author><?=$article->pa_name?></a></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>